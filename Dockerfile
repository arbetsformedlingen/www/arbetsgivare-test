FROM nginx:1.25.4-alpine3.18-slim

#COPY nginx/default.conf.template /etc/nginx/templates/default.conf.template
COPY default.conf /etc/nginx/conf.d

COPY public /usr/share/nginx/html

RUN    ln -sf /dev/stdout    /var/log/nginx/nginx-access.log \
    && ln -sf /dev/stderr    /var/log/nginx/nginx-error.log
EXPOSE 8080
STOPSIGNAL SIGQUIT


CMD ["nginx", "-g", "daemon off; error_log stderr info;"]
